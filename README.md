# bakkesmod-patch

```
Install or patch a BakkesMod plugin dll.

USAGE:
    bakkesmod-patch [FLAGS] [OPTIONS] <path>

FLAGS:
    -h, --help       Prints help information
    -V, --version    Prints version information
    -v, --verbose    Pass many times for more log output
                     
                     By default, it'll report info. Passing `-v` one time also prints debug, `-vv` enables trace.

OPTIONS:
    -p, --password <password>    The BakkesMod rcon password. [default: password]
    -s, --server <server>        Address of the BakkesMod rcon server. [default: ws://127.0.0.1:9002]
    -S, --sleep <sleep>          Number of milliseconds to wait for plugin to unload. [default: 500]

ARGS:
    <path>    Path to the plugin dll.
```
