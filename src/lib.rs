pub extern crate websocket;

use failure::Fail;
use log::info;
use std::{
    fs,
    io,
    net::TcpStream,
    path::{Path, PathBuf},
    thread,
    time::Duration,
};
use websocket::client::sync::Client as WSClient;
use winreg::{enums::HKEY_CURRENT_USER, RegKey};

#[derive(Debug, Fail)]
pub enum Error {
    #[fail(display = "No BakkesMod installation found.")]
    NoInstallation,
    #[fail(display = "Invalid host string: {}", _0)]
    InvalidHost(String),
    #[fail(display = "Failed to connect to BakkesMod: {}", _0)]
    Connection(#[cause] websocket::WebSocketError),
    #[fail(display = "Failed to authorize with BakkesMod: {}", _0)]
    Authorization(#[cause] websocket::WebSocketError),
    #[fail(display = "Communication error while replacing plugin: {}", _0)]
    Replacement(#[cause] websocket::WebSocketError),
    #[fail(display = "Plugin file does not exist: {}", _0)]
    NoPlugin(String),
    #[fail(display = "Plugin must be a dll: {}", _0)]
    InvalidPlugin(String),
    #[fail(display = "Io error: {}", _0)]
    Io(#[cause] io::Error),
}

pub type BMPResult<T = ()> = Result<T, Error>;

impl From<io::Error> for Error {
    fn from(e: io::Error) -> Error {
        Error::Io(e)
    }
}

#[macro_export]
macro_rules! ws_format {
    ($($arg:tt)*) => { $crate::websocket::Message::text(format!($($arg)*)) }
}

/// Searches the registry for the path to the BakkesMod installation.
///
/// Returns [`Error::NoInstallation`] if no installation is found.
pub fn find_bakkesmod_dir() -> BMPResult<PathBuf> {
    let hklm = RegKey::predef(HKEY_CURRENT_USER);
    let key = hklm
        .open_subkey(r"Software\BakkesMod\AppPath")
        .map_err(|_| Error::NoInstallation)?;

    key.get_value::<String, _>("BakkesModPath")
        .ok()
        .map(PathBuf::from)
        .filter(|p| p.is_dir())
        .ok_or(Error::NoInstallation)
}

/// Attempts to connect to BakkesMod rcon and returns a WebSocket stream if successful.
pub fn rcon_connect(url: &str, pass: &str) -> BMPResult<WSClient<TcpStream>> {
    let mut sock = websocket::ClientBuilder::new(url)
        .map_err(|e| Error::InvalidHost(e.to_string()))?
        .connect_insecure()
        .map_err(Error::Connection)?;

    sock.send_message(&ws_format!("rcon_password {}", pass))
        .map_err(Error::Connection)?;

    match sock.recv_message().map_err(Error::Connection)? {
        websocket::OwnedMessage::Text(ref msg) if msg == "authyes" => (),
        _ => {
            return Err(Error::Authorization(
                websocket::WebSocketError::ResponseError("Failed to authorize."),
            ))
        }
    }

    Ok(sock)
}

fn replace_plugin_file(path: &Path, plugins_dir: &Path) -> BMPResult {
    let destination = plugins_dir.join(
        path.file_name()
            .ok_or_else(|| Error::InvalidPlugin(path.to_string_lossy().into_owned()))?,
    );
    info!("Copying plugin to {}", destination.to_string_lossy());
    fs::copy(path, &destination)?;
    Ok(())
}

/// Attempts to install the plugin at `path`, sleeping for 500ms after unloading.
pub fn replace_plugin(path: &Path, server: &str, password: &str) -> BMPResult {
    replace_plugin_sleep(path, server, password, Duration::from_millis(500))
}

/// Attempts to install the plugin at `path`, sleeping for the given duration after unloading.
pub fn replace_plugin_sleep(
    path: &Path,
    server: &str,
    password: &str,
    sleep: Duration,
) -> BMPResult {
    if !path.is_file() {
        return Err(Error::NoPlugin(path.to_string_lossy().to_string()));
    }

    let extension = path.extension().map(|e| e.to_string_lossy().to_lowercase());

    if extension.filter(|e| e == "dll").is_none() {
        return Err(Error::InvalidPlugin(path.to_string_lossy().into_owned()));
    }

    let plugin_name = path
        .file_stem()
        .ok_or_else(|| Error::InvalidPlugin(path.to_string_lossy().into_owned()))?
        .to_string_lossy();

    let bakkesmod_dir = find_bakkesmod_dir()?;
    let plugins_dir = bakkesmod_dir.join("plugins");
    if !plugins_dir.is_dir() {
        fs::create_dir(&plugins_dir)?;
    }

    match rcon_connect(server, password) {
        Ok(mut sock) => {
            info!("Unloading {}", plugin_name);
            sock.send_message(&ws_format!("plugin unload {}", plugin_name))
                .map_err(Error::Replacement)?;
            thread::sleep(sleep);

            replace_plugin_file(path, &plugins_dir)?;

            info!("Loading {}", plugin_name);
            sock.send_message(&ws_format!("plugin load {}", plugin_name))
                .map_err(Error::Replacement)?;
            let _ = sock.send_message(&ws_format!("cl_settings_refreshplugins"));
        }
        // If no BakkesMod server was found, simply replace plugin file.
        Err(Error::Connection(websocket::WebSocketError::IoError(_))) => {
            replace_plugin_file(path, &plugins_dir)?;
        }
        other_err => {
            other_err?;
        }
    };

    Ok(())
}
