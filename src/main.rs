use bakkesmod_patch::*;
use log::{error, info};
use std::{io, path::PathBuf, process, time::Duration};
use structopt::StructOpt;

type FResult<T = ()> = Result<T, failure::Error>;

/// Install or patch a BakkesMod plugin dll.
#[derive(StructOpt)]
#[structopt(author = "")]
struct Opt {
    /// Path to the plugin dll.
    #[structopt(parse(from_os_str))]
    path: PathBuf,
    /// Address of the BakkesMod rcon server.
    #[structopt(short = "s", long = "server", default_value = "ws://127.0.0.1:9002")]
    server: String,
    /// The BakkesMod rcon password.
    #[structopt(short = "p", long = "password", default_value = "password")]
    password: String,
    /// Number of milliseconds to wait for plugin to unload.
    #[structopt(short = "S", long = "sleep", default_value = "500")]
    sleep: u64,
    /// Pass many times for more log output
    ///
    /// By default, it'll report info. Passing `-v` one time also prints
    /// debug, `-vv` enables trace.
    #[structopt(short = "v", long = "verbose", parse(from_occurrences))]
    verbose: u8,
}

fn run() -> FResult {
    let opt: Opt = Opt::from_args();

    let log_level = match opt.verbose {
        0 => simplelog::LevelFilter::Info,
        1 => simplelog::LevelFilter::Debug,
        _ => simplelog::LevelFilter::Trace,
    };
    simplelog::TermLogger::init(log_level, simplelog::Config::default())?;

    replace_plugin_sleep(
        &opt.path,
        &opt.server,
        &opt.password,
        Duration::from_millis(opt.sleep),
    )?;
    info!("Done");
    Ok(())
}

fn main() {
    let res = match std::panic::catch_unwind(run) {
        Ok(r) => r,
        Err(e) => {
            error!("Panic: {:?}", e);
            process::exit(1);
        }
    };

    if let Err(e) = res {
        error!("{}", e);
        for cause in e.iter_causes() {
            error!("Cause:\n\t{}", cause);
        }
        let code = e
            .downcast_ref::<io::Error>()
            .and_then(io::Error::raw_os_error)
            .unwrap_or(1);
        process::exit(code);
    }
}
